# Automatic build of the PageSpeed optimization module for Nginx

This projects aims to build a dynamic PageSpeed nginx module based on the [official source package](https://www.nginx.com/resources/wiki/start/topics/tutorials/install/) and the [PageSpeed documentation](https://modpagespeed.com/doc/build_ngx_pagespeed_from_source.)

## Install

```
echo "deb http://nginx.org/packages/ubuntu/ trusty nginx" > /etc/apt/sources.list.d/nginx.list
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys ABF5BD827BD9BF62
apt install -y apt-transport-https
echo "deb https://rollbrettler.gitlab.io/pagespeed-nginx-build/ trusty main" > /etc/apt/sources.list.d/pagespeed.list
apt update
apt install -y --force-yes nginx=1.10.3-1~trusty nginx-pagespeed-module
```

⚠️ The repository and the packages are not signed with a gpg key. Therefore the `--force-yes` flag is needed!
